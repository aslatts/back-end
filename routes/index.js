var express = require('express');
var router = express.Router();
var direction;
var sockets = [];

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});



function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}

function onConnection(socket) {
  sockets = sockets.push(socket);
  console.log(sockets);
  socket.emit('direction', direction);
  socket.on('goodbye', function(data){
    console.log(data);
  });

}

function updateSockets() {
  console.log('here');
  console.log(sockets.length)
  for(var i = 0; i < sockets.length; i++){
    console.log('updating sockets');
    sockets[i].emit('direction', direction);
  };
}

module.exports = router;
